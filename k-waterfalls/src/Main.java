import java.awt.Color;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Screen screen = new Screen();
		screen.width = 800;
		screen.height = 600;
		screen.title = "Waterfall";

		screen.drawRect(0, 0, 800, 600, Color.BLACK);
		screen.paint();

		ArrayList<Waterfall> waterfalls = new ArrayList<>();

		while (true) {

			Click mouseClick = screen.lastClick();
			screen.drawRect(0, 0, 800, 600, Color.BLACK);

			if (mouseClick != null) {

				Waterfall wf = new Waterfall();
				wf.x = mouseClick.x - Waterfall.WIDTH / 2;
				wf.y = mouseClick.y;
				wf.height = 0;
				Waterfall.color = Color.getHSBColor((float)Math.random(), .8f, 1f);

				waterfalls.add(wf);
			}

			for (int i = 0; i < waterfalls.size(); i++) {
				Waterfall wf = waterfalls.get(i);
				screen.drawRect(wf.x, wf.y, Waterfall.WIDTH, wf.height, Waterfall.color);
				
				if(wf.y + wf.height  < screen.height - 10) {
					wf.height = wf.height + wf.direction;
				} else {
					wf.height = wf.height -1;
					wf.direction = -1;
				}
				
			}

			screen.paint();
			screen.sleep(1);

		}

	}
}