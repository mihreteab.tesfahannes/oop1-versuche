import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

class PresentStationPanel extends JPanel {

	JTextField tfAmount = new JTextField("2");
	JLabel lblAmount = new JLabel("Packete pro Minute");
	JButton btApply = new JButton("OK");

	Present[] presents;
	int speed = 2;
	final int wrappingStationWidth = 100;

	public void init() {
		setLayout(null);

		// (1) mit .setBounds alle GUI Elemente positionieren
		// (2) mit this.add() Elemente dem Panel hinzufügen
		// (3) Present Klasse erstellen (mit allen Attributen)
		// (4) Present[] instanzieren (10 Elemente)
		// (5) Alle Presents instanzieren. Jeweils x,y, color setzen.
		// RandomColor.createRandomColor() verwenden.

		// Alle 50 [ms] feuert der Timer
		Timer timer = new Timer(50, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {

				// (6) Von allen presents die x-Koordinate um "speed" inkrementieren.
				// (7) Wenn Present über der Hälfte des Bildschirms: wrapped = true setzen
				// ... sonst wrapped = false setzen.
				// (8) Wenn Present ausserhalb der Bildschirms rechts ...
				// ... Present wieder an den Anfang links setzen

				repaint();
			}
		});
		timer.start();

		// (9) Actionlistener dem Button hinzufügen mit .addActionListener
		// (10) Wenn Button gedrückt wird: tfAmount.getText() als int parsen
		// ... und die Variable speed entsprechend setzen.

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		// (11) Hintergrund zeichnen (Light Gray)

		// (12) Alle Presents zeichnen (wenn presents != null)
		// ... Methode draw(Graphics g) in Present erstellen

		Toolkit.getDefaultToolkit().sync();

		// (13) Packstation zeichnen
		// (14) Fliessband zeichnen

	}
}