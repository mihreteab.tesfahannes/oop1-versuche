import java.awt.Dimension;

import javax.swing.JFrame;

public class App {

	public static void main(String[] args) {

		JFrame frame = new JFrame("Paketstation");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		PresentStationPanel panel = new PresentStationPanel();
		panel.setDoubleBuffered(true);
		panel.setPreferredSize(new Dimension(800, 600));
		frame.add(panel);
		frame.pack();

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(true);

		panel.init();
	}
}
