import java.awt.Color;

public class RandomColor {

	public static Color createRandomColor() {
		return Color.getHSBColor((float) Math.random(), 1f, 1f);
	}
}
