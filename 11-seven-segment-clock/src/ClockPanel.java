import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

public class ClockPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Clock clock = new Clock(10, 10);

	/**
	 * <pre>
	 * - erzeugt timer mit Periode 1000 und startet ihn
	 * - im Callback updateTime() von clock ausf�hren
	 * </pre>
	 */
	public void init() {

	}

	/**
	 * <pre>
	 * - setzt Hintergrundfarbe schwarz
	 * - clock zeichnen
	 * </pre>
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

	}
}
