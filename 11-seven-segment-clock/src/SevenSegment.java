import java.awt.Graphics;

public class SevenSegment extends GraphicalItem {

	/**
	 * <pre>
	 * - Setzt Attribute der Elternklasse
	 * - Erzeugt alle sieben Segmente gem�ss Skizze
	 * </pre>
	 */
	public SevenSegment(int x, int y) {
		super(x, y);

	}

	/**
	 * <pre>
	 *  
	 * - zeigt alle Segmente an
	 * </pre>
	 */
	public void draw(Graphics g) {

	}

	/**
	 * <pre>
	 * - setzt alle Segmente auf false
	 * - setzt Zustand der Segmente gem�ss zahl (0-9) auf true oder false
	 *   Hinweis: switch case Anweisung verwenden
	 * </pre>
	 */
	public void setzeWert(int zahl) {

	}

}
