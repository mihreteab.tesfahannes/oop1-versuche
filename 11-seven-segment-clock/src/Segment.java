import java.awt.Color;
import java.awt.Graphics;

public class Segment extends GraphicalItem {

	public static final int HORIZONTAL = 0;
	public static final int VERTIKAL = 1;

	protected int orientierung;
	protected boolean ein = false;

	/**
	 * <pre>
	 * - Setzt Attribute der Klasse und Elternklasse
	 * </pre>
	 */
	public Segment(int x, int y, int orientierung) {
		super(x, y);
	}

	/**
	 * <pre>
	 * - falls ein: Farbe auf rot setzen, sonst auf schwarz
	 * - f�r beide Orientierungen Segment zeichnen. hierzu jeweils:
	 * 	- int array der x Punkte des Segments erzeugen (Skizze)
	 * 	- int array der y Punkte des Segments erzeugen (Skizze)
	 * 	- mit fillPolygon das Segment zeichnen
	 * </pre>
	 */
	public void draw(Graphics g) {

	}

	/**
	 * <pre>
	 * - Attribut setzen
	 * </pre>
	 */
	public void setzeZustand(boolean ein) {

	}

}
