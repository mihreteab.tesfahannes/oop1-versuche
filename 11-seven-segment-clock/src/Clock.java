import java.awt.Color;
import java.awt.Graphics;
import java.util.GregorianCalendar;
import java.util.Calendar;

public class Clock extends GraphicalItem {
	

	/**
	 * <pre>
	 * - Setzt Attribute der Elternklasse
	 * - Erzeugt alle sechs siebenSegmente gem�ss Skizze
	 * </pre>
	 */
	public Clock(int x, int y) {
		super(x, y);
	}

	/**
	 * <pre>
	 * - zeigt alle siebenSegmente an
	 * - setzt Farbe auf rot
	 * - zeichnet mit fillRect alle vier Trennpunkte gem�ss Skizze
	 *   Hinweis: x Koordinaten (x + 124), (x + 248)
	 *            y Koordinaten (y + 32), (y +72)
	 * </pre>
	 */
	public void draw(Graphics g) {
		

	}

	/**
	 * <pre>
	 * - GregorianCalendar calendar = new GregorianCalendar()
	 * - stunde = calendar.get(Calendar.HOUR_OF_DAY)
	 * - minute = calendar.get(Calendar.MINUTE)
	 * - sekunde = calendar.get(Calendar.SECOND)
	 * - setzt alle 6 Ziffern entsprechend der Zeit
	 * </pre>
	 */
	public void updateTime() {
		

	}

}
