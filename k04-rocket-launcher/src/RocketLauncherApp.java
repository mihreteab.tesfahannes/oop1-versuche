import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.UIManager;

public class RocketLauncherApp {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		JFrame frame = new JFrame("Rocket Launcher");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final RocketLauncherPanel panel = new RocketLauncherPanel();
		panel.setPreferredSize(new Dimension(800, 600));
		panel.setDoubleBuffered(true);
		frame.add(panel);

		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		panel.init();
		panel.setFocusable(true);
	}

}
