import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import utils.ImageHelper;

public class RocketLauncherPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	// (c) Credits for Pixel Art Images to Robin Schneider, FHNW EIT HS20

	// 150px x 260px
	BufferedImage rocketThrust = ImageHelper.loadImage("res/rocket_thrust.png");
	// 150px x 260px
	BufferedImage rocketIdle = ImageHelper.loadImage("res/rocket_idle.png");

	public void init() {
		// Hier wird das Panel erstellt
		// GUI Komponenten und Listener hier erstellen und registrieren
	}

}
