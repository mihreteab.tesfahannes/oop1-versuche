import java.awt.Dimension;

import javax.swing.JFrame;

import util.ActivePanel;

public class PongBallApp {



	public static void main(String[] args) {
		JFrame frame = new JFrame("Pong Ball Turbo2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final ActivePanel panel = new PongBallPanel();
		panel.setPreferredSize(new Dimension(800, 600));
		panel.setDoubleBuffered(true);
		frame.add(panel);

		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		panel.onInit();
		panel.setFocusable(true);
		panel.requestFocus();
		
	}

}
