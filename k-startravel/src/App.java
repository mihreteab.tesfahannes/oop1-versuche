import java.awt.Color;
import java.util.ArrayList;

public class App {

	public static void main(String[] args) {

		Screen screen = new Screen();
		screen.width = 800;
		screen.height = 600;
		screen.title = "Startravel";

		ArrayList<Star> points = new ArrayList<>();

		for (int i = 0; i < 100; i++) {
			Star s = new Star();
			s.x = (int) (Math.random() * screen.width);
			s.y = (int) (Math.random() * screen.height);
			s.v = (int)(Math.random() * 10);
			points.add(s);
		}

		while (true) {

			screen.drawRect(0, 0, screen.width, screen.height, Color.BLACK);

			for (int i = 0; i < 100; i++) {
				Star star = points.get(i);
				star.x = star.x + star.v;
				// point.x = 0.9;

				if (star.x >= screen.width) {
					star.x = 0;
				}
				
				screen.drawCircle(star.x, star.y, 10, Color.WHITE);

			}

			screen.paint();

			screen.sleep(30);
		}

	}

}
