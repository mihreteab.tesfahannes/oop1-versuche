import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class Screen {
	private final Object lock = new Object[] {};
	private final Object clickLock = new Object[] {};

	private Click currentClick;
	private int updateCount;

	private abstract static class ScreenObject {
		final int x, y, updateId;
		final Color color;

		public ScreenObject(int updateId, int x, int y, Color color) {
			super();
			this.x = x;
			this.y = y;
			this.color = color;
			this.updateId = updateId;
		}

		abstract void draw(Graphics g);
	}

	private static class Circle extends ScreenObject {
		final int diameter;

		public Circle(int updateId, int x, int y, Color color, int diameter) {
			super(updateId, x, y, color);
			this.diameter = diameter;
		}

		@Override
		void draw(Graphics g) {
			g.setColor(color);
			int x = (int) (this.x - this.diameter / 2.0);
			int y = (int) (this.y - this.diameter / 2.0);
			g.fillOval(x, y, diameter, diameter);
		}
	}

	private static class Rect extends ScreenObject {
		final int width, height;

		public Rect(int updateId, int x, int y, Color color, int width, int height) {
			super(updateId, x, y, color);
			this.width = width;
			this.height = height;
		}

		@Override
		void draw(Graphics g) {
			g.setColor(color);
			// int x = (int) (this.x - this.width / 2.0);
			// int y = (int) (this.y - this.height / 2.0);
			g.fillRect(x, y, width, height);
		}
	}

	private static class Text extends ScreenObject {
		final Font font;
		final String text;

		public Text(int updateId, int x, int y, Color color, String text, int size) {
			super(updateId, x, y, color);
			this.text = text;
			this.font = new Font("sans", Font.PLAIN, size);
		}

		@Override
		void draw(Graphics g) {
			g.setColor(color);
			g.setFont(font);
			g.drawString(text, x, y);
		}
	}

	private final ArrayList<ScreenObject> screenObjects = new ArrayList<>();

	public int width = 640;
	public int height = 480;
	public String title = "Screen";

	private boolean initialized = false;

	/**
	 * Zeichnet ein Rechteck auf dem Screen
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param color
	 */
	public void drawRect(int x, int y, int width, int height, Color color) {
		synchronized (lock) {
			screenObjects.add(new Rect(updateCount, x, y, color, width, height));
		}
	}

	/**
	 * Zeichnet einen Kreis auf dem Bildschirm (x und y sind Zentrum)
	 * 
	 * @param x
	 * @param y
	 * @param diameter
	 * @param color
	 */
	public void drawCircle(int x, int y, int diameter, Color color) {
		synchronized (lock) {
			screenObjects.add(new Circle(updateCount, x, y, color, diameter));
		}
	}

	/**
	 * Zeichnet Text auf dem Bildschirm. Ankerpunkt ist oben links.
	 * 
	 * @param x
	 * @param y
	 * @param text
	 * @param size
	 * @param color
	 */
	public void drawText(int x, int y, String text, int size, Color color) {
		synchronized (lock) {
			screenObjects.add(new Text(updateCount, x, y, color, text, size));
		}
	}

	/**
	 * Löscht alle elemente auf dem Screen
	 */
	public void clear() {
		synchronized (lock) {
			screenObjects.clear();
		}
	}

	/**
	 * Wartet so lange, bis die Maus geclickt wurde.
	 * 
	 * @return
	 */
	public Click waitForClick() {
		synchronized (clickLock) {
			try {
				clickLock.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
				return null;
			}

			Click res = new Click(currentClick.x, currentClick.y);
			currentClick = null;
			return res;
		}
	}

	/**
	 * Wartet soviele Millisekunden bis der Screen angeclickt wurde. Falls in der
	 * Zeit kein Click stattgefunden hat, wird null zurückgegeben.
	 * 
	 * @param milliseconds
	 * @return
	 */
	public Click waitForClick(long milliseconds) {
		synchronized (clickLock) {
			try {
				clickLock.wait(milliseconds);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return null;
			}

			if (currentClick == null) {
				return null;
			}

			Click res = new Click(currentClick.x, currentClick.y);
			currentClick = null;
			return res;
		}
	}

	/**
	 * gibt den letzten Click zurück. falls nicht geclickt wurde gibt die Methode
	 * null zurück
	 * 
	 * @return
	 */
	public Click lastClick() {
		synchronized (clickLock) {
			if (currentClick == null) {
				return null;
			}

			Click res = new Click(currentClick.x, currentClick.y);
			currentClick = null;
			return res;
		}
	}

	/**
	 * Muss aufgerufen werden um zu zeichnen
	 */
	public void paint() {

		if (!initialized) {

			panel.setPreferredSize(new Dimension(width, height));
			frame.setTitle(title);

			frame.pack();
			frame.setVisible(true);
			frame.setResizable(false);
			frame.setLocationRelativeTo(null);

			panel.setDoubleBuffered(true);
			panel.setFocusable(true);
			panel.requestFocus();
			initialized = true;
		}

		synchronized (lock) {
			for (ScreenObject s : new ArrayList<ScreenObject>(screenObjects)) {
				if (s.updateId != updateCount) {
					screenObjects.remove(s);
				}
			}
			updateCount++;
		}


		panel.repaint();
	}

	@SuppressWarnings("serial")
	final JPanel panel = new JPanel() {
		final RenderingHints CONFIG = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			((Graphics2D) g).setRenderingHints(CONFIG);
			synchronized (lock) {
				for (ScreenObject sc : screenObjects) {
					sc.draw(g);
				}
			}
		};

		public void repaint() {
			super.repaint();
			Toolkit.getDefaultToolkit().sync();
		};
	};

	JFrame frame = new JFrame(title);

	public Screen() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);

		panel.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				synchronized (clickLock) {
					Screen.this.currentClick = new Click(e.getX(), e.getY());
					clickLock.notify();
				}
			}
		});
	}

	/**
	 * Programm wartet anzahl Millisekunden
	 * 
	 * @param timeInMs
	 */
	public void sleep(long timeInMs) {
		try {
			Thread.sleep(timeInMs);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

class Click {
	public final int x, y;

	public Click(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
