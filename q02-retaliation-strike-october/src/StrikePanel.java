import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JPanel;

import utils.ActivePanel;
import utils.ImageHelper;

public class StrikePanel extends ActivePanel {

	private static final long serialVersionUID = 1L;

	BufferedImage pumpkin = ImageHelper.loadImage("res/pumpkin.png");
	BufferedImage pumpkinStruck = ImageHelper.loadImage("res/pumpkin_struck.png");
	BufferedImage pumpkinStruck2 = ImageHelper.loadImage("res/pumpkin_struck_2.png");
	BufferedImage missileIdle = ImageHelper.loadImage("res/missile_idle_8x8.png");
	BufferedImage missileLaunched = ImageHelper.loadImage("res/missile_launched_8x8.png");
	BufferedImage missileIcon = ImageHelper.loadImage("res/missile_icon.png");
	BufferedImage city = ImageHelper.loadImage("res/city.png");
	BufferedImage background = ImageHelper.loadImage("res/background.png");

	int pumpkinX, pumpkinY;
	int missileX;

	double missileY = 0;
	double missileVy = 0;
	double missileAy = 0;

	boolean launched = false;

	long startTime, endTime;
	int score = 0;
	boolean started = false;
	int highscore = 100;
	int strikeCount = 0;

	void spawn() {
		// TODO pumpkinX und pumpkinY setzen
		// (alle Variablen existieren bereits)

		// pumpkinX: Zahl zwischen 0 und 700
		// pumpkinY: immer 100
	}

	boolean collide() {
		double dx = Math.abs(pumpkinX + 50 - (missileX + 50));
		double dy = Math.abs(pumpkinY + 100 - missileY);

		// TODO wenn dx < 50 ist und dy < 100
		// dann soll true zurückgegeben / returned werden
		// sonst false

		return false;
	}

	int getTime() {
		// TODO
		// Zeitdifferenz in Sekunden als int zurückgeben
		// startTime ist in Millisekunden
		// endTime ist in Millisekunden
		return 9999;
	}

	void reset() {
		// TODO
		// (alle Variablen existieren bereits)
		// launched auf false setzen
		// missileAy auf 0 setzen
		// missileVy auf 0 setzen
		// missileY auf die Höhe des Panels setzen
		// missileX auf 350 setzen
	}

	void init() {
		spawn();
		reset();
	}

	void drawCity(Graphics g) {
		// TODO
		// das Bild city ganz links unten Zeichnen
		// es ist 100 Pixel hoch
	}

	void drawPumpkin(int x, int y, Graphics g) {
		// TODO
		// Bild pumpkin an x, y zeichnen
		// pumpkin ist 100 breit und 100 hoch
	}

	void drawMissile(int x, int y, Graphics g) {
		// TODO
		// Zeichne Missile an der Stelle x und y - 100
		// Die Missile ist 100x100 Pixel gross
		// Falls die Variable launched wahr ist
		// verwende das Image missileLaunched
		// sonst
		// verwende das Image missileIdle

	}

	void drawWelcomeScreen(Graphics g) {
		// TODO
		// Zeichne den Welcome Screen
		// Weissen Text "Presss SPACE to start"
		// Etwas in der Mitte des Bildschirms anzeigen
	}

	void drawGameScreen(Graphics g) {
		// TODO
		// drawPumpkin mit pumpkinX und pumpkinY aufrufen

		// Punkte (Variable score) oben rechts
		// mit weisser Schrift zeichnen.
		// Umwandlung von int zu String mit: ""+score

		// drawMissile aufrufen
		// im Aufruf missileY zu int casten
	}

	void drawHighscoreScreen(Graphics g) {

		// In weisem Text drei Zeilen anzeigen
		// Seconds: 15 | hier getTime() verwenden
		// Highscore: 14 | hier highscore verwenden
		// Press SPACE to restart
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setFont(new Font("monospaced", Font.PLAIN, 50));

		// TODO
		// zeichne das Image "background" an der Stelle 0, 0
		// breite: so breit wie panel
		// höhe: so hoch wie panel

		// TODO drawCity aufrufen

		// TODO
		// wenn Variable started true ist
		// drawGamescreen aufrufen
		// sonst, wenn score >= 10
		// drawHighScoreScreen aufrufen
		// sonst
		// drawWelcomeScreen aufrufen
	}

	@Override
	public void onKeyDown(int keyCode) {

		switch (keyCode) {
		case KeyEvent.VK_SPACE:
			if (!started) {
				started = true;
			} else {
				launched = true;
			}
			break;
		case KeyEvent.VK_LEFT:
			missileX -= 50;
			break;
		case KeyEvent.VK_RIGHT:
			missileX += 50;
			break;
		default:
			break;
		}
		// missileX und missileY sind bereits im Klassenrumpf deklariert
		// started, launched, score, und startTime auch

		// TODO A

		// Wenn die SPACE Taste gedrückt wurde
		// Wenn started false ist
		// Dann started auf true setzen
		// und score auf 0 setzen
		// und die Methode spawn aufrufen
		// und startTime auf System.currentTimeMillis()

		// ==============================================

		// TODO B
		// Wenn die SPACE Taste gedrückt aber started true ist
		// launched auf true setzen
		// und missileAy 5 setzen

		// ==============================================

		// TODO C

		// Wenn linke Pfeiltaste gedrückt ist
		// 1. missileX um 50 dekrementieren
		// 2. Wenn missileX kleiner 0 ist, dann missileX 0 setzen

		// Wenn rechte Pfeiltatste gedrückt ist
		// 1. missileX um 50 inkrementieren
		// 2. wenn missileX grösser als getWidth() -100 ist
		// dann missileX getWidth() - 100 setzen

		// ==============================================

		// TODO D

		// ganz Am Schluss Panel immer neu zeichnen mit repaint
	}

	@Override
	public void onTick() {
		// Hier in onTick nichts machen, einfach so lassen wie es ist.
		// Black Magic!

		if (strikeCount > 0) {
			strikeCount--;
			if (strikeCount == 0) {
				spawn();
			}
		}

		if (launched) {
			missileVy += Math.pow(missileAy, 2) * 1 / 100;
			missileY -= missileVy;
			if (missileY < 0) {
				reset();
			}

			if (collide()) {
				strikeCount = 10;
				score++;
				reset();
			}

			if (score >= 10) {
				reset();
				started = false;
				endTime = System.currentTimeMillis();
				if (getTime() < highscore) {
					highscore = getTime();
				}
			}
		}

		repaint();
	}
}
