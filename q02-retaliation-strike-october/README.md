# Prüfung 2: Retaliaton Strike October

![Screen](./screens/screen.png)

In diesem Projekt implementieren Sie das Spiel "Retaliation Strike October". **Folgen Sie den Anweisungen Schritt für Schritt um den besten Erfolg zu erzielen**.

## Zeit: 60 Minuten

## Erlaubte Hilfsmittel

- Sie dürfen eigene Übungen verwenden.
- Cheatsheet von OOP1 ist erlaubt.
- Slides von OOP1 sind erlaubt
- Keine Kommunikation während der Prüfung.
- Kein Abschreiben.
- Keine sonstigen Hilfsmittel.

## Vorgehensweise

Viele Teile der Applikation sind bereits im Code vorgegeben. Implementieren Sie die zusätzlichen Teile gemäss Vorgehen unten. Methoden sollen gemäss Pseudocode im Source implementiert werden.

Implementieren Sie die Klasse `StrikePanel.java` gemäss der Aufgabenstellung. Senden Sie den Inhalt von `StrikePanel.java` als E-Mail (als Anhang oder Inhalt) an die Adresse `manuel.dicerbo@fhnw.ch`.

1. Starten Sie `StrikeApp.java` um zu verifizieren, dass Sie das Projekt importieren können.
2. Öffnen Sie `StrikePanel.java`, hier Implementieren Sie den Code.
3. Folgen Sie Schritt für Schritt der Aufgabe.
4. Formatieren Sie Ihren Code regelmässig mit `Shift + Ctrl + F`.

## Wichtig: `drawImage`

Für `g.drawImage` gibt es verschiende Methoden. Verwenden Sie folgende Methode, falls Breite und Höhe verlangt werden:

```java
drawImage(Image img, int x, int y, int width, int height, ImageObserver observer)
```

Sie beinhaltet auch Breite und Höhe für das Bild um dieses zu skalieren.
Für Observer können Sie immer `null` verwenden.

## Wichtig `onTick`

Die Methdoe `onTick` ist bereits fertig implementiert, hier müssen Sie nichts ändern.

## 1. `paintComponent`

Implementieren Sie `paintComponent` gemäss Pseudocode.

## 2. `drawWelcomeScreen`

Implementieren Sie die Methode `drawWelcomeScreen`.

![Screen](./screens/welcome.png)

## 3. `drawCity`

Implementieren Sie `drawCity` gemäss Pseudocode.

![Screen](./screens/welcome_city.png)

## 4. `onKeyDown` Teil A und D

Um das Spiel zu starten, soll der User "SPACE" drücken. Auch halten wir den Startzeitpunkt fest um am Ende anzuzeigen wie lange der User hatte um das Spiel zu beenden.

Implementieren Sie Teil `A` und Teil `D` von `onKeyDown`.

Verifizieren Sie, ob das Spiel nun in den nächsten Modus springt, nachdem Sie SPACE drücken (der Welcome Text verschwindet jetzt zumindest).

### WICHTIG !

Implementieren Sie Teil `D` von `onKeyDown`. Sonst wird nach dem Keypress nicht neu gezeichnet!


## 5. `spawn()` und `reset()`

Implementieren Sie `spawn` und `reset` gemäss Pseudocode.

## 6. Pumpkin zeichnen `drawPumpkin`

Implementieren Sie die Methode `drawPumpkin`. Verwenden Sie hierbei das Image `pumpkin`.

## 7. Implementieren Sie `drawGameScreen`

Implementieren Sie die Methode `drawGameScreen` gemäss Pseudocode.

Sie sollten nach dem drücken von Space nun auch Pumpkin auf dem Bild sehen.

![Screen](./screens/gamescreen_1.png)

## 8. Implementieren Sie `drawMissile`

Implementieren Sie die Methode `drawMissile` gemäss Pseudocode.

![Screen](./screens/gamescreen_2.png)

## 9. Missile starten in `onKeyDown`

Implementieren Sie `Teil B` in `onKeyDown` um die Missile zu starten.
Mit SPACE können Sie jetzt die Rakete starten!

## 10. Links und Rechts navigieren in `onKeyDown`

Implementieren Sie `Teil C` in `onKeyDown` um die Rakte nach Links und Rechts zu navigieren.
Verifzieren Sie das Resultat!

## 11. Collision Detection `collide`

Implementieren Sie die Methode `collide` gemäss Pseudocode.

Gratulation! Das Spiel ist funktionial! Wenn Sie 10 mal Pumpkin treffen erscheint nun der High Score Screen.

## 12. `drawHighscoreScreen`

Implementieren Sie die Methode `drawHighscoreScreen` gemäss Pseudocode.

Wenn Sie das Spiel fertig ist, sehen Sie aber, dass etwas mit der Score noch nicht stimmt.

## `getTime`

Implementieren Sie die Methode `getTime` gemäss Pseudocode. Testen Sie nochmals. Casten Sie wenn nötig nach `int`.


## Kosmetik

### Missile ist in der Luft steuerbar

Ändern Sie Ihren Code so, dass die Missile in der Luft nicht mehr steuerbar ist. Tipp: Testen Sie in `onKeyDown` auf `launched`.

### Explosion

Pumpkin soll explodieren.

Ändern Sie `drawPunkpkin` so ab, dass wenn die Variable `strikeCount` grösser als 7 ist, `pumpkinStruck` verwendet wird, wenn sie grösser als `5` ist `pumpkinStruck2` verwendet wird und sonst `pumpkin` verwendet wird.


## Fertig

Gratulation! Sie haben Retaliation Strike October implementiert! Die Stadt ist heute ein Stück sicherer geworden!