import java.awt.Color;
import java.awt.Graphics;

import util.ActivePanel;

public class ForestPanel extends ActivePanel {

	private static final long serialVersionUID = 1L;

	private static final Color STEM_BROWN_1 = Color.decode("#8b3700");
	private static final Color STEM_BROWN_2 = Color.decode("#b30000");
	private static final Color STEM_BROWN_3 = Color.decode("#4d1a00");

	private static final Color FOLIAGE_GREEN_1 = Color.decode("#0c5000");
	private static final Color FOLIAGE_GREEN_2 = Color.decode("#408000");
	private static final Color FOLIAGE_GREEN_3 = Color.decode("#00b300");

	private static final Color GRASS_GREEN = Color.decode("#99ff33");

	@Override
	public void onInit() {
		// TODO was passiert, wenn das Frame startet?
		repaint();
	}

	public void drawTree(int x, int y, Graphics g) {
		// TODO drawTree implementieren
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// TODO drawTree aufrufen
	}

}
