import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import utils.ImageHelper;

public class TreasureIslandPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	Image hintergrund = ImageHelper.loadImage("res/sealife.jpg");

	Lobster redLobster = new Lobster(100, 100, Color.RED);
	Lobster greenLobster = new Lobster(400, 300, Color.GREEN);

	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(hintergrund, 0, 0, 800, 600, null);

		redLobster.draw(g);
		greenLobster.draw(g);
	}

	public void init() {

		MouseMotionListener listener = new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();

				redLobster.x = x;
				greenLobster.y = y;

				repaint();

			}

			@Override
			public void mouseDragged(MouseEvent e) {

			}
		};

		addMouseMotionListener(listener);
	}

}
