import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import utils.ImageHelper;

public class HelloPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private final BufferedImage rakete = ImageHelper.loadImage("res/rocket-idle.png");

	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// TODO
		// Farbe auf weiss setzen
		// Gefüllten Kreis bei 300,200 mit Radius 100 zeichnen
		// Rakete in der Mitte des Bildschirms zeichnen
		// Tipp: verwenden Sie .getWidth() und .getHeight() von rakete
		
		// TODO Bonus
		// Bonus: 1000 sterne 1x1 px Zeichnen (im Hintergrund)
		
	}
}
