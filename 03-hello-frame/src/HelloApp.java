import javax.swing.JFrame;

public class HelloApp {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Hello Application");
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new HelloPanel());

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
