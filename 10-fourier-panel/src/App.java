import java.awt.Dimension;

import javax.swing.JFrame;

public class App {
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Fourier Panel");

		FourierPanel panel = new FourierPanel();
		panel.setPreferredSize(new Dimension(650, 800));
		panel.setDoubleBuffered(true);
		panel.setFocusable(true);
		frame.add(panel);
		frame.pack();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		panel.requestFocus();
		panel.init();
	}
}
