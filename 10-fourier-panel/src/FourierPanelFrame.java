import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

class FourierPanel extends JPanel {

	private JTextField tfAmp = new JTextField("1.0");
	private JTextField tfFreq = new JTextField("1.0");
	private JTextField tfHarm = new JTextField("10");
	private JComboBox<String> cbForm = new JComboBox<String>();
	private JButton btBerechne = new JButton("Berechne");
	private PlotPanel plotPanel = new PlotPanel();
	private double[] signal = {};

	/**
	 * <pre>
	 * - Baut GUI 
	 * - Registriert Listener
	 * </pre>
	 */
	public void init() {
		setLayout(null);
	}

	/**
	 * <pre>
	 * - Liest Information aus GUI aus und legt sie in entsprechenden lokalen Var. ab.
	 * - Verzweigung cbForm.getSelectedIndex():
	 *   - Fall 0:
	 *     - signal gleich berechneRechteck() mit entsprechenden Parametern berechnen.
	 *   - Fall 1:
	 *     - signal gleich berechneDreieck() mit entsprechenden Parametern berechnen.
	 *   - Fall 2:
	 *     - signal gleich berechneSaegezahn() mit entsprechenden Parametern berechnen.
	 *   - default:
	 *     - signal gleich berechneRechteck() mit entsprechenden Parametern berechnen.
	 * - Setzt Signal des PlotPanels mittels setSignal().
	 * - L�st Neuzeichnen aus.
	 * </pre>
	 */
	public void update() {
		int N = 620; // Anzahl Punkte auf der x Achse

	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Rechtecksignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneRechteck(double amp, double freq, int nHarm, int N) {
		double[] y = new double[N];

		return y;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Dreiecksignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneDreieck(double amp, double freq, int nHarm, int N) {
		double[] y = new double[N];

		return y;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Saegezahnsignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneSaegezahn(double amp, double freq, int nHarm, int N) {
		double[] y = new double[N];

		return y;
	}

}