import java.awt.Graphics;

import javax.swing.JPanel;

import util.IFlashListener;

public class FlashPanel extends JPanel implements IFlashListener {

	private static final long serialVersionUID = 1L;

	// TODO "globale" Variablen hier deklarieren

	@Override
	public void onTick() {
		// TODO was passiert alle 16ms?
	}

	@Override
	public void onMouseDown(int x, int y) {
		// TODO Was passiert, wenn die Maus an x,y gedrückt wird?
	}

	@Override
	public void onMouseUp(int x, int y) {
		// TODO Was passiert, wenn der Mausbutton losgelassen wird?
		// currentSector = -1;
	}

	@Override
	public void onInit() {
		// TODO was passiert, wenn das Frame startet?
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		// TODO hier wird gezeichnet
	}

}
