import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class FlashApp {

	private static void repaint(JPanel panel) {
		// Panel wird neu gezeichnet
		panel.repaint();
		// Render Pipeline flushen (Performance)
		Toolkit.getDefaultToolkit().sync();
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Super Color Flash");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final FlashPanel panel = new FlashPanel();
		panel.setPreferredSize(new Dimension(800, 600));
		panel.setDoubleBuffered(true);
		frame.add(panel);

		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		panel.onInit();

		new Timer(16, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.onTick();
				repaint(panel);
			}
		}).start();

		panel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				panel.onMouseUp(e.getX(), e.getY());
				repaint(panel);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				panel.onMouseDown(e.getX(), e.getY());
				repaint(panel);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// ignore
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// ignore
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// ignore
			}
		});
	}

}
