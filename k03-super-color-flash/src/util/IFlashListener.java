package util;

public interface IFlashListener {

	/**
	 * Wird alle 16 ms ausgeführt. Später wird ein repaint ausgeführt.
	 */
	void onTick();

	/**
	 * Wird ausgeführt, wenn die linke Maustaste nach unten gedrückt wird. Später
	 * wird ein repaint ausgeführt.
	 * 
	 * @param x x Koordinate des Mouseclicks
	 * @param y y Koordinate des Mouseclicks
	 */
	void onMouseDown(int x, int y);

	/**
	 * Wird ausgeführt, wenn die linke Maustaste losgelassen wird. Später wird ein
	 * repaint ausgeführt.
	 * 
	 * @param x x Koordinate des Mouseclicks
	 * @param y y Koordinate des Mouseclicks
	 */
	void onMouseUp(int x, int y);

	/**
	 * Wird ausgeführt nachdem das Fenster erstellt wurde.
	 */
	void onInit();
}
