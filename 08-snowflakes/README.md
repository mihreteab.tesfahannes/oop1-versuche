# Snowflakes

In dieser Übung animieren wir Schneeflocken.

![](./doc/snowflakes.gif)

In der Applikation werden Schneeflocken gezeichnet, die von oben nach unten fallen.

Mittels Keypress auf die Pfeiltasten nach oben und unten wird die Anzahl Schneeflocken erhöht oder verringert.

Mittels Keypress auf "Space" / "Leertaste" werden die Schneeflocken alle wieder über den Bildschirmrand neu positioniert.

## Klassendiagramm

Das Projekt folgt folgendem Klassendiagramm

![](./doc/class-diagram.png)

### Die Klassen `Sprite` und `Position`

Die Klasse `Position` verfügt nur über eine `x` und `y` Koordinate. `Sprite` verfügt über eine Breite, Höhe und eine Bildresource. Sie repräsentiert ein Bild das gezeichnet werden soll. Sprite selber verwendet inter ` Utility.loadResourceImage(path)` um Bilder zu laden wie z.B. das Hintergundbild oder die Schneeflocke.

## Hintergrund zeichnen

- Instanzieren Sie in `SnowflakePanel` das Attribut `background` gemäss Klassendiagramm. Als Konstruktorparameter können Sie `x = 0, y = 0, width = 900, height = 800` übergeben.
- Überschreiben Sie `paintComponent` von `SnowflakePanel`, und zeichnen Sie `background`. Rufen Sie vorher auch `super.paintComponent(g)` auf.


## Klasse `Snowflake`

Erstellen Sie die Klasse `Snowflake` gemäss Klassendiagramm. Die Klasse erbt von `Sprite`. Im Konstruktor wird auch die `vy` mitgegeben. Wichtig: rufen Sie im Konstruktor als erstes den `super` Konstruktor auf.

## Schneeflocken zeichnen

Wir zeichnen auf dem Bildschirm zuerst zufällig 200 Schneeflocken.

### Attribut `snowflakes` in `SnowflakePanel`

- Erstellen Sie das Attribut `snowflakes` in `SnowflakePanel` mit Platz für 200 Elemente.

### Konstruktor von `SnowflakePanel`

Im Konstruktor von `SnowflakePanel` sollen nun alle Schneeflocken instanziert werden.

- Instanzieren Sie alle Schneeflocken
- ... verwenden Sie als Kostruktorparameter `x = 0, y = 0, width = 10, height = 10, path = "snowflake.png"`.


### Methode `reposition()`

- Erstellen Sie eine Methode `reposition`, welche alle Schneeflocken zufällig auf dem Bildschirm anordnet.
- Rufen Sie die Methode nun im Kostruktor auf, nachdem Sie alle Schneeflocken instanziert haben.


### `paintComponent()`

- Zeichnen Sie alle Schneeflocken.

## Schneeflocken fallen lassen

Um die Schneeflocken zu bewegen benötigen wir einen Timer, welcher in regelmässigen Abständen alle Schneeflocken etwas nach unten bewegt und ein Neuzeichnen des Bildschirms auslöst.

### Timer und `timerAction()`

Wir verwenden `SimpleTimer` und `SimpleTimerListener` für diesen Verusch um periodisch die Positionen der Schneeflocken neu zu berechnen.

- `SnowflakePanel` soll SimpleTimerListener ***implementieren*** . Das heisst, die Methode `timerAction()` soll gemäss Klassendiagramm deklariert werden.
- In `timerAction` sollen alle `y`-Attribute der Schneeflocken jeweils um deren `vy`-Attribut inkrementiert werden. Falls eine Flocke über den Bildschirm hinausgeht, dann soll die Schneeflocke wieder über den Bildschirm gesetzt werden.
- ... verwenden Sie folgende Logik: neues `y` ist altes `y` modulo `HEIGHT` und minus die Höhle der Schneeflocke.
- Schlussendlich soll `repaint()` noch aufgerufen werden.

### Timer registrieren

Registrieren Sie den Timer nun im Konstruktor von `SnowflakePanel`.

- Erstellen Sie hierzu eine neue Instanz von `SimpleTimer` mit den Attributen `30` und `this`.
- Starten Sie das erstellte Timer-Objekt mit `start`.

### `reposition()`

- Ändern Sie die Methode so, dass die Schneeflocken zuerst über dem Bildschirm (bis zu `-HEIGHT`) positioniert werden gemäss Skizze:

![Offscreen](./doc/offscreen.png)


Wenn Sie die Applikation starten, sollte es anfangen zu schneien!

## Schneestärke einstellen

Mittels Tastendruck auf `UP` / `DOWN` soll die Stärke des Schnees eingestellt werde können.

Diese (Anzahl Schneeflocken) wird auf dem Panel oben links angzeigt.

### Attribut `count` und `MAX`

- Erstellen Sie das Attribut `MAX` gemäss Klassendiagramm. Hierbei soll `MAX` `final` sein. Es sind die maximal möglichen Schneeflocken.
- Erstellen Sie auch das Attribut `count` und initialisieren Sie dieses auf `MAX` um am Anfang den stärksten Schneesturm anzuzeigen.
- Passen Sie das `Snowflakes[]` so an, dass gleich am Anfang `MAX` Schneeflocken im Array platz haben.

### `paintComponent()`

- Es sollen nur noch die `count` ersten Schneeflocken gezeichet werden. Passen Sie `paintComponent` dementsprechend an.
- Zeichnen Sie oben Links auf dem Panel die Anzahl Schneeflocken (vergleich Screenshot oben).

### `KeyListener`

- Implemetieren Sie in `SnowflakePanel` das interface `KeyListener`. Hierzu müssen Sie die drei Methoden `keyPressed`, `keyTyped` und `keyReleased` implementieren. Wir werden allerdings nur `keyPressed` verwenden, die restlichen Methoden können leer gelassen werden.
- In `keyPressed` soll via `switch` case `keyEvent.getKeyCode()` überprüft werden.
- Im Falle von `KeyEvent.VK_UP` soll das Attribut `count` um eins erhöht werden.
- Im Falle von `KeyEvent.VK_DOWN` soll `count` um eins verringert werden.
- `count` darf aber in keinem Fall kleiner als 0 sein, oder grösser als `MAX`.
- Registrieren Sie den KeyListener im Konstruktor von `SnowflakePanel` via `addKeyListener(this)`.

Sie sollten nun in der Lage sein, den Schneefall via UP/DOWN Pfeiltasten zu steuern.

## Schneeflocken neu positionieren wenn `SPACE` gedrückt wird

### `keyPressed()`

Ändern Sie `keyPressed()` in `SnowflakePanel` so ab, dass die Schneeflocken neu über dem Bildschirmrand positioniert werden.


