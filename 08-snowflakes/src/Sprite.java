import java.awt.Graphics;
import java.awt.Image;

import util.Utility;

public class Sprite extends Position {

	private Image image;

	private int width;
	private int height;

	public Sprite(int x, int y, int width, int height, String path) {
		super(x, y);
		this.width = width;
		this.height = height;
		this.image = Utility.loadResourceImage(path);
	}

	public void draw(Graphics g) {
		g.drawImage(image, x, y, null);
	}

}
