import java.awt.Color;
import java.awt.Graphics;

public class Circle extends Shape {

	public Circle(int x, int y, int size, Color color) {
		super(x, y, size, color);		
	}
	
	public void draw(Graphics g) {
		super.draw(g);
		g.fillOval(this.x, this.y, this.size, this.size);
	}

}
