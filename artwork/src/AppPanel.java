import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class AppPanel extends JPanel {

	private Shape[] shapes = new Shape[100];

	private Shape circle = new Circle(0, 0, 100, Color.RED);

	public void init() {

		setLayout(null);

		if (circle instanceof Circle) {
			Circle c = (Circle) circle;
		}

		// für i = 0 bis Länge von shapes
		// instanziere shapes an der stelle i
		for (int i = 0; i < shapes.length; i++) {
			float hue = (float) Math.random();
			Color c = Color.getHSBColor(hue, 1f, 1f);

			int x = (int) (Math.random() * getWidth());
			int y = (int) (Math.random() * getHeight());
			int size = (int) (Math.random() * 100);

			Shape s;

			if (Math.random() > 0.5) {
				s = new Circle(x, y, size, c);
			} else {
				s = new Square(x, y, size, c);
			}
			shapes[i] = s;
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		for (Shape s : shapes) {
			s.draw(g);
		}
	}
}
