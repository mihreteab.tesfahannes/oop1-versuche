import java.awt.Dimension;

import javax.swing.JFrame;

public class App {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Artwork");

		AppPanel panel = new AppPanel();
		panel.setPreferredSize(new Dimension(800, 600));
		panel.setDoubleBuffered(true);
		panel.setFocusable(true);
		frame.add(panel);
		frame.pack();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		panel.requestFocus();
		panel.init();
	}

}
