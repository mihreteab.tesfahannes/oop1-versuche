import java.awt.Color;
import java.awt.Graphics;

public class Shape {
	
	public int x, y, size;
	public Color color;
	
	
	public Shape(int x, int y, int size, Color color) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.color = color;
	}
	
	public void draw(Graphics g) {
		g.setColor(this.color);
	}
}
