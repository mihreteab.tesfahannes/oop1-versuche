import java.awt.Color;
import java.awt.Graphics;

public class Square extends Shape {

	
	public Square(int x, int y, int size, Color color) {
		super(x, y, size, color);
	}

	public void draw(Graphics g) {
		super.draw(g);
		g.fillRect(this.x, this.y, this.size, this.size);
	}

}

