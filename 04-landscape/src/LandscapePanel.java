import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import utils.ImageHelper;

public class LandscapePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	// Wolke, mit g.drawImage(x,y,w,h,null) zeichnen
	private final BufferedImage cloud = ImageHelper.loadImage("res/cloud.png");

	// Black magic, Antialias
	private final static RenderingHints CONFIG = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
			RenderingHints.VALUE_ANTIALIAS_ON);

	// Farben, welche Sie mit g.setColor verwenden können
	private static final Color BROWN = Color.decode("#8b3700");
	private static final Color TREE_GREEN = Color.decode("#0c5000");

	public void init() {
		// TODO Timer starten
	}

	@Override
	protected void paintComponent(Graphics g) {
		// Black Magic, Antialiasing
		((Graphics2D) g).setRenderingHints(CONFIG);

		// TODO zeichnen Sie!

	}
}
