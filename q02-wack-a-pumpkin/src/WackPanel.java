import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JPanel;

import utils.ImageHelper;

public class WackPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	BufferedImage image = ImageHelper.loadImage("res/pumpkin.png");

	int pumpkinX, pumpkinY;
	long startTime, endTime;
	int smacked = 0;
	boolean started = false;
	int highscore = 100;

	void spawn() {
		// TODO
		int x = 0; // ein int x zufällig zwischen 0 und 4 wählen
		int y = 100; // ein int y zufällig zwischen 0 und getHeight() - 200 wählen

		pumpkinX = x * 200 + 50;
		pumpkinY = y;
	}

	void smack(int corridor) {
		int pumpkinCorridor = pumpkinX / 200;
		if (pumpkinCorridor == corridor) {
			smacked++;
			spawn();
		}
	}

	int getTime() {
		// TODO
		// Zeit in Sekunden als int zurückgeben
		// startTime ist in Millisekunden
		// endTime ist in Millisekunden
		return 9999;
	}

	void corridorButtonPressed(int corridor) {
		if (!started) {
			smacked = 0;
			started = true;
			startTime = System.currentTimeMillis();
		} else {
			smack(corridor);
		}

		if (smacked >= 20) {
			started = false;
			endTime = System.currentTimeMillis();
			if (getTime() < highscore) {
				highscore = getTime();
			}
		}

		repaint();
	}

	void init() {
		spawn();
		setLayout(null);

		for (int i = 0; i < 4; i++) {
			final int corridor = i;
			int x = i * 200;
			int y = getHeight() - 100;
			int w = 200;
			int h = 100;

			// TODO
			// 1. JButton "SMACK" erstellen
			// 2. mit add() dem Panel hinzufügen
			// 3. setBounds mit x,y und breite w / höhe h setzen
			// 4. ActionListener hinzufügen
			// 5. Wenn der Knopf gedrückt wird, soll corridorButtonPressed aufgerufen werden
			// und als Argument die Variable corridor übergeben werden

		}
	}

	void drawWelcomeScreen(Graphics g) {
		// TODO
		// weissen Text zeichnen: "Press any Button"
		// ungefähr in der Bildschirmmitte
	}

	void drawGameScreen(Graphics g) {
		// TODO
		// 1. pumpkin zeichnen
		// an der Stelle, pumpkinX, pumpkinY
		// mit Breite 100 und Höhe 100
		// 2. Text Zeichnen oben rechts am Bildschirm
		// mit Anzahl "smacked" pumpkins (Variable smacked)

	}

	void drawHighscoreScreen(Graphics g) {
		// TODO
		// Highscore Screen zeichnen
		// g.drawString für Text verwenden
		// weisse Farbe verwenden
		// 1. Zeile: "Seconds: 10", getTime() verwenden um Sekunden zu bekommen
		// 2. Zeile: "Highscore: 9", Variable highscore verwenden
		// 3. Zeile: "Press any Button to restart"
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setFont(new Font("monospace", Font.PLAIN, 50));

		// TODO zeichne schwarzen Hintergrund
		// TODO zeichne weisses Hintergrundpannel unten für die vier Knöpfe, 100px hoch

		if (started) {
			drawGameScreen(g);
		} else if (smacked > 0) {
			drawHighscoreScreen(g);
		} else {
			drawWelcomeScreen(g);
		}

	}
}
