package screen;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Screen {
	private final Object lock = new Object[] {};
	private final Object clickLock = new Object[] {};
	private final Object moveLock = new Object[] {};
	private final Object keyLock = new Object[] {};

	private final static Object[] imageLock = new Object[] {};
	private final static HashMap<String, BufferedImage> images = new HashMap<String, BufferedImage>();
	private final static Font mainFont;

	static {
		Font font = null;
		InputStream is = ClassLoader.getSystemResourceAsStream("font/font.ttf");

		try {
			font = Font.createFont(Font.TRUETYPE_FONT, is);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mainFont = font;
	}

	private Click currentClick;
	private int currentKeyCode;
	private MousePosition currentPosition = new MousePosition(0, 0);

	private int updateCount;

	private abstract static class ScreenObject {
		final int x, y, updateId;
		final double rotation;
		final Color color;

		public ScreenObject(int updateId, int x, int y, Color color, double rotation) {
			super();
			this.x = x;
			this.y = y;
			this.rotation = rotation;
			this.color = color;
			this.updateId = updateId;
		}

		abstract void draw(Graphics2D g);
	}

	private static class Circle extends ScreenObject {
		final int diameter;

		public Circle(int updateId, int x, int y, Color color, int diameter) {
			super(updateId, x, y, color, 0);
			this.diameter = diameter;
		}

		@Override
		void draw(Graphics2D g) {
			g.setColor(color);
			int x = (int) this.x;
			int y = (int) this.y;
			g.fillOval(x, y, diameter, diameter);
		}
	}

	private static class Rect extends ScreenObject {
		final int width, height;

		public Rect(int updateId, int x, int y, Color color, int width, int height, double rotation) {
			super(updateId, x, y, color, rotation);
			this.width = width;
			this.height = height;
		}

		@Override
		void draw(Graphics2D g) {
			g.setColor(color);
			g.fillRect(x, y, width, height);
		}
	}

	private static class Text extends ScreenObject {
		final Font font;
		final String text;

		public Text(int updateId, int x, int y, Color color, String text, int size, double rotation) {
			super(updateId, x, y, color, rotation);
			this.text = text;
			this.font = mainFont.deriveFont((float) size);
		}

		@Override
		void draw(Graphics2D g) {
			g.setColor(color);
			g.setFont(font);
			g.drawString(text, x, y);
		}
	}

	private static class Line extends ScreenObject {
		final int x2, y2;
		final Stroke stroke;

		public Line(int updateId, int x1, int y1, int x2, int y2, int thickness, Color color) {
			super(updateId, x1, y1, color, 0);
			this.x2 = x2;
			this.y2 = y2;
			this.stroke = new BasicStroke(thickness);
		}

		@Override
		void draw(Graphics2D g) {
			g.setColor(color);
			g.setStroke(stroke);
			g.drawLine(x, y, x2, y2);
		}
	}

	public static class Bitmap extends ScreenObject {

		final int width, height;
		final BufferedImage image;
		final boolean flip;

		public Bitmap(int updateId, String name, int x, int y, int width, int height, boolean flip, double rotation) {
			super(updateId, (int) x, y, Color.BLACK, rotation);
			this.width = width;
			this.height = height;
			this.flip = flip;

			synchronized (imageLock) {
				if (images.get(name) != null) {
					image = images.get(name);
				} else {
					InputStream is = ClassLoader.getSystemResourceAsStream(String.format("images/%s.png", name));
					BufferedImage img = null;

					try {
						img = ImageIO.read(is);
					} catch (IOException e) {
						System.out.printf("can't load image (res/images/): %s\n", name);
					}

					image = img;
					images.put(name, img);
				}
			}
		}

		@Override
		void draw(Graphics2D g) {
			if (image == null) {
				return;
			}

			g.rotate(rotation, x + width / 2, y + height / 2);

			if (!flip) {
				g.drawImage(image, x, y, width, height, null);
			} else {
				g.drawImage(image, x + width, y, -width, height, null);
			}

			g.rotate(-rotation, x + width / 2, y + height / 2);
		}

	}

	private final ArrayList<ScreenObject> screenObjects = new ArrayList<>();

	public int width = 640;
	public int height = 480;
	public String title = "Screen";

	private boolean initialized = false;

	/**
	 * Zeichnet ein Rechteck auf dem Screen
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param color
	 */
	public void addRect(double x, double y, double width, double height, Color color) {
		synchronized (lock) {
			screenObjects.add(new Rect(updateCount, (int) x, (int) y, color, (int) width, (int) height, 0));
		}
	}

	/**
	 * Zeichnet ein Rechteck auf dem Screen
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param color
	 * @param rotation rotation in rad
	 */
	public void addRect(double x, double y, double width, double height, Color color, double rotation) {
		synchronized (lock) {
			screenObjects.add(new Rect(updateCount, (int) x, (int) y, color, (int) width, (int) height, rotation));
		}
	}

	/**
	 * Zeichnet einen Kreis auf dem Bildschirm (x und y sind Zentrum)
	 * 
	 * @param x
	 * @param y
	 * @param diameter
	 * @param color
	 */
	public void addCircle(double x, double y, double diameter, Color color) {
		synchronized (lock) {
			screenObjects.add(new Circle(updateCount, (int) x, (int) y, color, (int) diameter));
		}
	}

	/**
	 * Zeichnet Text auf dem Bildschirm. Ankerpunkt ist oben links.
	 * 
	 * @param x
	 * @param y
	 * @param text
	 * @param size
	 * @param color
	 */
	public void addText(double x, double y, String text, double size, Color color) {
		synchronized (lock) {
			screenObjects.add(new Text(updateCount, (int) x, (int) y, color, text, (int) size, 0.0));
		}
	}

	public void addText(double x, double y, String text, double size, Color color, double rotation) {
		synchronized (lock) {
			screenObjects.add(new Text(updateCount, (int) x, (int) y, color, text, (int) size, rotation));
		}
	}

	public void addSprite(String name, double x, double y, double width, double height) {
		addSprite(name, x, y, width, height, false, 0.0);
	}

	public void addSprite(String name, double x, double y, double width, double height, double rotation) {
		addSprite(name, x, y, width, height, false, rotation);
	}

	public void addSprite(String name, double x, double y, double width, double height, boolean flip) {
		addSprite(name, x, y, width, height, flip, 0.0);
	}

	public void addSprite(String name, double x, double y, double width, double height, boolean flip, double rotation) {
		synchronized (lock) {
			try {
				screenObjects.add(
						new Bitmap(updateCount, name, (int) x, (int) y, (int) width, (int) height, flip, rotation));
			} catch (Exception e) {
				System.err.printf("error loading sprite, please make sure it's stored in " + "res > images\n"
						+ "res/images/%s.png\n" + "file names are case sensitive\n"
						+ "refresh the project with right click > refresh\n", name);

				System.exit(1);
			}
		}
	}

	public void addLine(double x1, double y1, double x2, double y2, double thickness, Color color) {
		synchronized (lock) {
			screenObjects.add(new Line(updateCount, (int) x1, (int) y1, (int) x2, (int) y2, (int) thickness, color));
		}
	}

	/**
	 * Löscht alle elemente auf dem Screen
	 */
	public void clear() {
		synchronized (lock) {
			screenObjects.clear();
		}
	}

	/**
	 * Wartet so lange, bis die Maus geclickt wurde.
	 * 
	 * @return
	 */
	public Position waitForClick() {
		synchronized (clickLock) {
			try {
				clickLock.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
				return null;
			}

			Position res = new Position(currentClick.x, currentClick.y);
			currentClick = null;
			return res;
		}
	}

	/**
	 * Wartet soviele Millisekunden bis der Screen angeclickt wurde. Falls in der
	 * Zeit kein Click stattgefunden hat, wird null zurückgegeben.
	 * 
	 * @param milliseconds
	 * @return
	 */
	public Position waitForClick(long milliseconds) {
		synchronized (clickLock) {
			try {
				clickLock.wait(milliseconds);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return null;
			}

			if (currentClick == null) {
				return null;
			}

			Click res = new Click(currentClick.x, currentClick.y, currentClick.button);
			currentClick = null;
			return res;
		}
	}

	/**
	 * gibt den letzten Click zurück. falls nicht geclickt wurde gibt die Methode
	 * null zurück
	 * 
	 * @return
	 */
	public Position lastClick() {
		synchronized (clickLock) {
			if (currentClick == null) {
				return null;
			}

			Position res = new Position(currentClick.x, currentClick.y);
			currentClick = null;
			return res;
		}
	}

	public MousePosition mousePosition() {
		synchronized (moveLock) {
			return new MousePosition(currentPosition.x, currentPosition.y);
		}
	}

	/**
	 * gibt den letzten KeyCode zurück. falls nichts gedrückt wurde gibt die Methode
	 * -1 zurück
	 * 
	 * Vergleichen mit z.B: KeyEvent.VK_UP für Taste nach oben
	 * 
	 * @return
	 */
	public int waitForKeyPress(long milliseconds) {
		synchronized (keyLock) {
			try {
				keyLock.wait(milliseconds);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return -1;
			}

			int r = currentKeyCode;
			currentKeyCode = -1;
			return r;
		}
	}

	/**
	 * gibt die Taste zurück, welche als letztes gedrückt wurde oder -1, falls keine
	 * Taste gedrückt worden ist.
	 * 
	 * Vergleichen mit z.B: KeyEvent.VK_UP für Taste nach oben
	 * 
	 * 
	 * @return
	 */
	public int lastKeyCode() {
		synchronized (keyLock) {
			int r = currentKeyCode;
			currentKeyCode = -1;
			return r;
		}
	}

	/**
	 * Muss aufgerufen werden um zu zeichnen
	 */
	public void paint() {

		if (!initialized) {

			panel.setPreferredSize(new Dimension(width, height));
			frame.setTitle(title);

			frame.pack();
			frame.setVisible(true);
			frame.setResizable(false);
			frame.setLocationRelativeTo(null);

			panel.setDoubleBuffered(true);
			panel.setFocusable(true);
			panel.requestFocus();
			initialized = true;
		}

		synchronized (lock) {
			for (ScreenObject s : new ArrayList<ScreenObject>(screenObjects)) {
				if (s.updateId != updateCount) {
					screenObjects.remove(s);
				}
			}
			updateCount++;
		}

		panel.repaint();
	}

	@SuppressWarnings("serial")
	final JPanel panel = new JPanel() {
		final RenderingHints CONFIG = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			((Graphics2D) g).setRenderingHints(CONFIG);
			synchronized (lock) {
				for (ScreenObject sc : screenObjects) {
					sc.draw((Graphics2D) g);
				}
			}
		};

		public void repaint() {
			super.repaint();
			Toolkit.getDefaultToolkit().sync();
		};
	};

	private JFrame frame;

	public Screen() {

		final Object[] lock = new Object[] {};

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {

				frame = new JFrame(title);

				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.add(panel);

				panel.addMouseListener(new MouseAdapter() {

					@Override
					public void mousePressed(MouseEvent e) {
						synchronized (clickLock) {
							Screen.this.currentClick = new Click(e.getX(), e.getY(),
									e.getButton() == MouseEvent.BUTTON1 ? 0 : 1);
							clickLock.notify();
						}
					}
				});

				panel.addMouseMotionListener(new MouseMotionAdapter() {
					@Override
					public void mouseMoved(MouseEvent e) {
						synchronized (moveLock) {
							Screen.this.currentPosition.update(e.getX(), e.getY());
							moveLock.notify();
						}
					}
				});

				panel.addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						synchronized (keyLock) {
							Screen.this.currentKeyCode = e.getKeyCode();
							keyLock.notify();
						}
					}
				});

				panel.setFocusable(true);
				panel.requestFocus();

				synchronized (lock) {
					lock.notify();
				}

			}
		});

		synchronized (lock) {
			try {
				lock.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Programm wartet anzahl Millisekunden
	 * 
	 * @param timeInMs
	 */
	public void sleep(long timeInMs) {
		try {
			Thread.sleep(timeInMs);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static class Position {
		public final int x, y;

		public Position(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	public static class Click extends Position {

		public static final int BUTTON_LEFT = 0;
		public static final int BUTTON_RIGHT = 1;

		public final int button;

		public Click(int x, int y, int button) {
			super(x, y);
			this.button = button;
		}

		public boolean isButtonLeft() {
			return button == BUTTON_LEFT;
		}

		public boolean isButtonRight() {
			return button == BUTTON_RIGHT;
		}
	}

	public static class MousePosition {
		public int x, y;

		public MousePosition(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public void update(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

}
