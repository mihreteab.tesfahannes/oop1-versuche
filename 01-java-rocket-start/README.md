# Java Rocket Start


<img src="./rocket start.png" style="max-width: 100%; width: 300px"></img>

In diesem Versuch geht es darum ein erstes mal Source Code zu sehen. Hier springen wir sozusagen ins kalte Wasser.

## Projekt Importieren

Laden Sie das Projekt als ZIP herunter über gitlab in dem Sie diesem Link folgen:

### Herunterladen von Gitlab als ZIP

[Java Rocket Start ZIP](https://gitlab.fhnw.ch/oop/oop1-versuche/-/archive/master/oop1-versuche-master.zip?path=01-java-rocket-start)

Das ZIP müssen Sie ***nicht*** entpacken.

Tipp: Das Git repository finden Sie übrigens hier: [https://gitlab.fhnw.ch/oop/oop1-versuche/](https://gitlab.fhnw.ch/oop/oop1-versuche/)

### Importieren des Projekts

In Eclipse wählen Sie `Datei > Importieren ...`.
Selektieren Sie `Existing Project into Workspace`.

<img src="./import 1.png" style="max-width: 100%; width: 300px"></img>


Importieren Sie nun das heruntergeladene ZIP File (siehe Screenshot).


<img src="./import 2.png" style="max-width: 100%; width: 300px"></img>


Wählen Sie "Finish".

<img src="./done.png" style="max-width: 100%; width: 800px"></img>


### 









