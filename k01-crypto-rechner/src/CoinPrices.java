
public class CoinPrices {
	
	public static final double USD_PRICE_IN_CHF = 0.92;

	// Kürzel hier: https://en.wikipedia.org/wiki/List_of_cryptocurrencies
	
	public static final double RATE_USD_BCH = 572.61;
	public static final double RATE_USD_BTC = 44993.80;
	public static final double RATE_USD_ETH = 3064.49;
	public static final double RATE_USD_EOS = 4.12;
	public static final double RATE_USD_LTC = 153.24;
	public static final double RATE_USD_XMR = 257.65;
	
}
