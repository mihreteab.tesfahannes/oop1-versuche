package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReader {

	public static String readString() {
 
		final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			String tmp = "";
			
			while((tmp = reader.readLine()) != null) {
				if(!tmp.trim().equals("")) {
					break;
				}
			}
			final String res = tmp;
			return res;
		} catch (IOException e) {
			System.err.println("Fehler beim lesen");
			try {
				reader.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return "";
		}
		
		
	}

	public static double readDouble() {
		return Double.valueOf(readString());
	}

	public static int readInt() {
		return Integer.valueOf(readString());
	}
}
