
public class Tree {

	int x;
	int size = 128;
	double prob = 0.05;
	int level = 0;

	public void flake() {
		if (Math.random() < prob) {
			level++;
			level = Math.min(level, 4);
		}
	}
}
