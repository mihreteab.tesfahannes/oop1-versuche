import java.awt.Dimension;

import javax.swing.JFrame;

public class App {
	
	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Snowflakes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		SnowFlakesPanel panel = new SnowFlakesPanel();
		panel.setDoubleBuffered(true);
		panel.setPreferredSize(new Dimension(800,600));
		frame.add(panel);
		frame.pack();

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
		
		panel.init();
	}

}
