import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SnowFlakesPanel extends JPanel {

	final static int OFFSET = 200;
	Image snowflake;
	Image[] tree_images = new Image[5];
	Tree[] trees = new Tree[(int) (Math.random() * 5) + 1];

	Snowflake[] flakes = new Snowflake[100];

	double snowed = 0.0;
	int num = 50;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, OFFSET, getWidth(), getHeight() - OFFSET);

		for (Tree t : trees) {
			if (t == null) {
				continue;
			}
			g.drawImage(tree_images[t.level], t.x, getHeight() - t.size, t.size, t.size, null);
		}

		for (int i = 0; i < num; i++) {
			Snowflake f = flakes[i];
			g.drawImage(snowflake, f.x, f.y, null);
		}

		g.setColor(Color.WHITE);
		g.fillRect(0, (int) (getHeight() - snowed), getWidth(), (int) snowed + 1);
		Toolkit.getDefaultToolkit().sync();
	}

	public void init() {

		for (int i = 0; i < flakes.length; i++) {
			flakes[i] = new Snowflake();
			flakes[i].x = (int) (Math.random() * getWidth());
			flakes[i].y = (int) (Math.random() * (getHeight() - OFFSET)) + OFFSET;
		}

		try {
			snowflake = ImageIO.read(new File("./res/snowflake.png"));
			for (int i = 0; i < 5; i++) {
				tree_images[i] = ImageIO.read(new File("./res/tree_0" + (i + 1) + ".png"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < trees.length; i++) {
			Tree tree = new Tree();
			tree.x = (int) (Math.random() * getWidth());
			tree.size = 64 + (int) (Math.random() * 4) * 16;
			trees[i] = tree;
		}

		setLayout(null);

		JLabel lblTitle = new JLabel("Snowflakes");
		lblTitle.setHorizontalAlignment(JLabel.CENTER);
		lblTitle.setBounds(200, 50, 400, 20);
		add(lblTitle);

		JSlider slider = new JSlider();
		slider.setBounds(50, 70, 700, 50);
		add(slider);

		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				// alt + shift + r = refactor->rename
				num = slider.getValue();
			}
		});

		Timer timer = new Timer(100, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// timer ticked
				// flake nach unten fallen lassen

				for (int i = 0; i < num; i++) {
					Snowflake f = flakes[i];
					f.y += 1;
					if (f.y > getHeight()) {
						f.y = OFFSET;
						snowed += 0.2;
						for (Tree t : trees) {
							t.flake();
						}
					}
				}

				repaint();
			}
		});
		timer.start();
	}

}
